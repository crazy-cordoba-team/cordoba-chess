# Contributing guide

For branching and commit convention please read the following rules defined in the [wiki][4] of the repository.

# Contribution guide

## Rules

 - Every developer, whenever it is possible, should work in his own branch.
 - Never push directly to the development, staging or master branch.
 - All branches must be added into development branch through a merge request.
 - The code of your branch must be added to development branch through a merge request that must be approved for a manteiner or any other developer requested as a reviewer.
 - Only development branch is merged into staging branch.
 - Only staging branch is merged into master branch.
 - All branches must have a prefix indicating the purpose of the branch and this branch must be added to the develop branch with a single commit, you must squash your commits in orther to do this, that indicates that porpuses with a simple and descriptive message. This naming convention can follow the [commit convention rules][1].
 - All commits must follow the [commit convention rules][1], for more info visit your [official web site][2]. The scopes in this repository can be, `frontend`, `backend` or `root`.
 - When you introduces a **breaking** change please follow this [rule][3].
 - When you intruduce a commit that must be in the changelog of the repo please in the third line of the commit add the **#changelog** text in order to extract this commit in the next changelog file.
 - In every commit all tests and lint must pass, if not the branch will not merged until it is fixed.

## Naming convention

 - The name of variables, functions, classes, etc **must be descriptive** and should **never be abbreviated**.
 - In **typescript** when you use **declare** keyword in order to declare a **const** or **read-only** variable the name must be in uppercase eg: **_declare const NAME = 'my_name'_**

### Frontend: _Quasar framework_

 - Component file name and name property **must be in lowercase _separating words with dashes_**.
 - Page and Layouts files name and name properties **must be in pascalcase** _eg: NameOfPage_.
 - Layouts always **must be finish with _layout_ word** eg: _MyLayout_.

[1]: https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#type
[2]: https://commitlint.js.org/#/
[3]: https://www.conventionalcommits.org/en/v1.0.0-beta.2/#summary
[4]: https://gitlab.com/crazy-cordoba-team/cordoba-chess/-/wikis/Branch-and-commit-rules
