import { Document } from 'mongoose';
import { UserDto } from 'src/user/dtos/user.dto';
import { ChessEventDto } from './chess-event.dto';

export class ScheduleNestedDto extends Document {
  public owner: UserDto;
  public student: UserDto[];
  public chessEvent: ChessEventDto;
  public deletedBy?: UserDto;
}
