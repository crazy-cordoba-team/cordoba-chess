import { IsDate, IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { BonusType } from "../constants/bonus-type.constant";

export class CreateBonusDto {
  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsString()
  public description?: string;

  @IsEnum(BonusType)
  public type: BonusType;

  @IsNumber()
  @IsNotEmpty()
  public amount: number;

  @IsString()
  public chessEvent?: string;

  @IsDate()
  @IsNotEmpty()
  public startDate: Date;

  @IsDate()
  @IsNotEmpty()
  public endDate: Date;
}
