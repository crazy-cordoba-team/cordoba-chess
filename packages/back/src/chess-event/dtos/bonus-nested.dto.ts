import { Document } from 'mongoose';
import { UserDto } from 'src/user/dtos/user.dto';
import { ChessEventDto } from './chess-event.dto';

export class BonusNestedDto extends Document {
  public chessEvent?: ChessEventDto;
  public deletedBy?: UserDto;
}
