import { IsDate, IsEnum, IsNotEmpty, IsNumber, IsObject, IsString } from 'class-validator';
import { Document } from 'mongoose';
import { BonusType } from "../constants/bonus-type.constant";

export class BonusDto extends Document {
  @IsString()
  public id?: string;

  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsString()
  public description?: string;

  @IsEnum(BonusType)
  public type: BonusType;

  @IsNumber()
  @IsNotEmpty()
  public amount: number;

  @IsString()
  public chessEvent?: string;

  @IsDate()
  @IsNotEmpty()
  public startDate: Date;

  @IsDate()
  @IsNotEmpty()
  public endDate: Date;

  @IsDate()
  public createdAt?: Date;

  @IsDate()
  public updatedAt?: Date;

  @IsDate()
  public deletedAt?: Date;

  @IsString()
  public deletedBy?: string;
}
