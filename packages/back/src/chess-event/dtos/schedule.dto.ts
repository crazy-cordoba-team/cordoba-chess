import { IsArray, IsDate, IsObject, IsString } from 'class-validator';
import { Document } from 'mongoose';
import { DaysAndHoursDto } from "./days-and-hours.dto";

export class ScheduleDto extends Document {
  @IsString()
  public id?: string;

  @IsObject()
  public owner: string;

  @IsArray()
  public student: string[];

  @IsObject()
  public daysAndHours: DaysAndHoursDto;

  @IsString()
  public chessEvent: string;

  @IsDate()
  public createdAt?: Date;

  @IsDate()
  public updatedAt?: Date;

  @IsDate()
  public deletedAt?: Date;

  @IsString()
  public deletedBy?: string;
}
