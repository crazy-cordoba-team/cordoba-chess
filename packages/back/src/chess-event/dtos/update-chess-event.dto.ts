import { IsArray, IsDate, IsEnum, IsNotEmpty, IsNumber, IsObject, IsString } from 'class-validator';
import { UpdatePublicationDto } from 'src/publication/dtos/update-publication.dto';
import { ChessEventType } from "../constants/chess-event-type.constant";
import { IntervalType } from '../constants/interval-type.constant';
import { CreateScheduleDto } from "./create-schedule.dto";

export class UpdateChessEventDto extends UpdatePublicationDto {
  @IsDate()
  @IsNotEmpty()
  public startDate: Date;

  @IsDate()
  @IsNotEmpty()
  public endDate: Date;

  @IsDate()
  @IsNotEmpty()
  public inscriptionStart: Date;

  @IsDate()
  @IsNotEmpty()
  public inscriptionEnd: Date;

  @IsString()
  @IsNotEmpty()
  public duration: string;

  @IsEnum(ChessEventType)
  @IsNotEmpty()
  public type: ChessEventType;

  @IsNumber()
  @IsNotEmpty()
  public cost: number;

  @IsObject()
  @IsNotEmpty()
  public schedule: CreateScheduleDto;

  @IsArray()
  public bonuses?: string[];

  @IsNumber()
  public interval?: number;

  @IsEnum(IntervalType)
  public intervalType?: IntervalType;

  @IsNumber()
  public canonFada?: number;
}
