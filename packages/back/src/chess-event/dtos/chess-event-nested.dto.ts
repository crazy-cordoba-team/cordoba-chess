import { PublicationNestedDto } from '../../publication/dtos/publication-nested.dto';
import { BonusDto } from './bonus.dto';
import { ScheduleDto } from "./schedule.dto";

export class ChessEventNestedDto extends PublicationNestedDto {
  public schedule?: ScheduleDto;
  public bonuses?: BonusDto[];
}
