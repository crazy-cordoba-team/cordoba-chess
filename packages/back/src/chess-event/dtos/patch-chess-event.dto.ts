import { IsArray, IsDate, IsEnum, IsNumber, IsObject, IsString } from 'class-validator';
import { PatchPublicationDto } from '../../publication/dtos/patch-publication.dto';
import { ChessEventType } from "../constants/chess-event-type.constant";
import { IntervalType } from '../constants/interval-type.constant';
import { CreateScheduleDto } from "./create-schedule.dto";

export class PatchChessEventDto extends PatchPublicationDto {
  @IsDate()
  public startDate?: Date;

  @IsDate()
  public endDate?: Date;

  @IsDate()
  public inscriptionStart?: Date;

  @IsDate()
  public inscriptionEnd?: Date;

  @IsString()
  public duration?: string;

  @IsEnum(ChessEventType)
  public type?: ChessEventType;

  @IsNumber()
  public cost?: number;

  @IsObject()
  public schedule?: CreateScheduleDto;

  @IsArray()
  public bonuses?: string[];

  @IsNumber()
  public interval?: number;

  @IsEnum(IntervalType)
  public intervalType?: IntervalType;

  @IsNumber()
  public canonFada?: number;
}
