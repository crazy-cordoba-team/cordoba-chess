import { IsMilitaryTime, IsNotEmpty, IsObject } from 'class-validator';

class StartEnd {
  @IsMilitaryTime()
  @IsNotEmpty()
  start: string;

  @IsMilitaryTime()
  @IsNotEmpty()
  end: string;
}

export class DaysAndHoursDto {
  @IsObject()
  sunday: StartEnd;

  @IsObject()
  monday: StartEnd;

  @IsObject()
  tuesday: StartEnd;

  @IsObject()
  wednesday: StartEnd;

  @IsObject()
  thursday: StartEnd;

  @IsObject()
  friday: StartEnd;

  @IsObject()
  saturday: StartEnd;
}
