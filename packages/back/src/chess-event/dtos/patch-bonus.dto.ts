import { IsDate, IsEnum, IsNumber, IsString } from 'class-validator';
import { BonusType } from "../constants/bonus-type.constant";

export class PatchBonusDto {
  @IsString()
  public name?: string;

  @IsString()
  public description?: string;

  @IsEnum(BonusType)
  public type?: BonusType;

  @IsNumber()
  public amount?: number;

  @IsString()
  public chessEvent?: string;

  @IsDate()
  public startDate?: Date;

  @IsDate()
  public endDate?: Date;
}
