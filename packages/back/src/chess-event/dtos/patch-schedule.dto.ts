import { IsArray, IsObject, IsString } from 'class-validator';
import { DaysAndHoursDto } from "./days-and-hours.dto";

export class PatchScheduleDto {
  @IsObject()
  public owner?: string;

  @IsArray()
  public student?: string[];

  @IsObject()
  public daysAndHours?: DaysAndHoursDto;

  @IsString()
  public chessEvent?: string;
}
