import { Schema } from 'mongoose';
import { AbstractPublicationContentSchema } from '../../publication/schemas/publication-content.schema';
import { ChessEventType } from '../constants/chess-event-type.constant';
import { IntervalType } from '../constants/interval-type.constant';
import { ScheduleSchema } from './schedule.schema';


export const ChessEventSchema = new AbstractPublicationContentSchema({
  startDate: { type: Date, required: true },
  endDate: { type: Date, required: true },
  inscriptionStart: { type: Date, required: true },
  inscriptionEnd: { type: Date, required: true },
  duration: { type: String, required: true },
  type: { type: String, required: true, enum: Object.values(ChessEventType) },
  cost: { type: Number, required: true },
  schedule: { type: ScheduleSchema, required: true },
  bonuses: [{ type: Schema.Types.ObjectId, ref: 'Bonus' }],
  interval: Number,
  intervalType: { type: String, enum: Object.values(IntervalType) },
  canonFada: Number,
}, {
  timestamps: true,
});
