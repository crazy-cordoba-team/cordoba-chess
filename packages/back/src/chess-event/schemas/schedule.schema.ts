import { Schema } from 'mongoose';


export const ScheduleSchema = new Schema({
  owner: { type: Schema.Types.ObjectId, ref: 'User' },
  students: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  daysAndHours: Schema.Types.Mixed,
  chessEvent: { type: Schema.Types.ObjectId, ref: 'ChessEvent' },
}, {
  timestamps: true,
});
