import { Schema } from 'mongoose';
import { BonusType } from '../constants/bonus-type.constant';

export const BonusSchema = new Schema({
  name: { type: String, required: true },
  description: String,
  type: { type: String, required: true, enum: Object.values(BonusType) },
  amount: { type: Number, required: true },
  chessEvent: { type: Schema.Types.ObjectId, ref: 'ChessEvent' },
  startDate: { type: Date, required: true },
  endDate: { type: Date, required: true },
}, {
  timestamps: true,
});
