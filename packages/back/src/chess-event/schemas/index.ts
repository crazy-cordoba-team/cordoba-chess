import { BonusSchema } from "./bonus.schema";
import { ScheduleSchema } from "./schedule.schema";
import { ChessEventSchema } from "./chess-event.schema";

export default [
  { name: 'Bonus', schema: BonusSchema },
  { name: 'Schedule', schema: ScheduleSchema },
  { name: 'ChessEvent', schema: ChessEventSchema },
];
