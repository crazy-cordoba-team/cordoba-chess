import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post, Put, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { NestedController } from 'src/common/controllers/nested.controller';
import { BonusNestedDto } from '../dtos/bonus-nested.dto';
import { BonusDto } from '../dtos/bonus.dto';
import { CreateBonusDto } from '../dtos/create-bonus.dto';
import { PatchBonusDto } from '../dtos/patch-bonus.dto';
import { UpdateBonusDto } from '../dtos/update-bonus.dto';
import { BonusService } from '../services/bonus.service';


@ApiTags('Bonuses')
@Controller('bonuses')
export class BonusesController extends NestedController<
  CreateBonusDto,
  UpdateBonusDto,
  PatchBonusDto,
  BonusDto,
  BonusNestedDto> {
  constructor(public bonusService: BonusService) {
    super();
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() bonus: CreateBonusDto): Promise<BonusDto> {
    return this.bonusService.create(bonus);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  find(@Query() query?: object): Promise<BonusDto[]> {
    return this.bonusService.find(query);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findById(@Param('id') id: string): Promise<BonusDto> {
    return this.bonusService.findById(id);
  }


  @Get()
  @HttpCode(HttpStatus.OK)
  findOne(@Query() query?: object): Promise<BonusDto> {
    return this.bonusService.findOne(query);
  }

  @Get(':id/:nested')
  @HttpCode(HttpStatus.OK)
  findNestedDocuments(@Param('id') id: string, @Param('nested') nested: string, @Query() query?: object): Promise<BonusNestedDto> {
    return this.bonusService.findNested(id, nested, query);
  }

  @Put(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  update(@Param('id') id: string, @Body() bonus: UpdateBonusDto): Promise<BonusDto> {
    return this.bonusService.update(id, bonus);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  updateProperty(@Param('id') id: string, @Body() partialUser: PatchBonusDto): Promise<BonusDto> {
    return this.bonusService.updateProperty(id, partialUser);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  deleteDocument(@Param('id') id: string): Promise<void> {
    return this.bonusService.deleteDocument(id);
  }

  @Patch('restore/:id')
  @HttpCode(HttpStatus.ACCEPTED)
  restoreDocument(@Param('id') id: string): Promise<void> {
    return this.bonusService.restoreDocument(id);
  }
}
