import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post, Put, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { NestedController } from 'src/common/controllers/nested.controller';
import { CreateScheduleDto } from '../dtos/create-schedule.dto';
import { PatchScheduleDto } from '../dtos/patch-schedule.dto';
import { ScheduleNestedDto } from '../dtos/schedule-nested.dto';
import { ScheduleDto } from '../dtos/schedule.dto';
import { UpdateScheduleDto } from '../dtos/update-schedule.dto';
import { ScheduleService } from '../services/schedule.service';


@ApiTags('Schedules')
@Controller('schedules')
export class SchedulesController extends NestedController<
  CreateScheduleDto,
  UpdateScheduleDto,
  PatchScheduleDto,
  ScheduleDto,
  ScheduleNestedDto> {
  constructor(public scheduleService: ScheduleService) {
    super();
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() schedule: CreateScheduleDto): Promise<ScheduleDto> {
    return this.scheduleService.create(schedule);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  find(@Query() query?: object): Promise<ScheduleDto[]> {
    return this.scheduleService.find(query);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findById(@Param('id') id: string): Promise<ScheduleDto> {
    return this.scheduleService.findById(id);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  findOne(@Query() query?: object): Promise<ScheduleDto> {
    return this.scheduleService.findOne(query);
  }

  @Get(':id/:nested')
  @HttpCode(HttpStatus.OK)
  findNestedDocuments(@Param('id') id: string, @Param('nested') nested: string, @Query() query?: object): Promise<ScheduleNestedDto> {
    return this.scheduleService.findNested(id, nested, query);
  }

  @Put(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  update(@Param('id') id: string, @Body() schedule: UpdateScheduleDto): Promise<ScheduleDto> {
    return this.scheduleService.update(id, schedule);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  updateProperty(@Param('id') id: string, @Body() partialUser: PatchScheduleDto): Promise<ScheduleDto> {
    return this.scheduleService.updateProperty(id, partialUser);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  deleteDocument(@Param('id') id: string): Promise<void> {
    return this.scheduleService.deleteDocument(id);
  }

  @Patch('restore/:id')
  @HttpCode(HttpStatus.ACCEPTED)
  restoreDocument(@Param('id') id: string): Promise<void> {
    return this.scheduleService.restoreDocument(id);
  }
}
