import { BonusesController } from "./bonuses.controller";
import { SchedulesController } from "./schedules.controller";
import { ChessEventsController } from "./chess-events.controller";

export default [
  BonusesController,
  SchedulesController,
  ChessEventsController,
];
