import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post, Put, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { NestedController } from '../../common/controllers/nested.controller';
import { ChessEventNestedDto } from '../dtos/chess-event-nested.dto';
import { ChessEventDto } from '../dtos/chess-event.dto';
import { CreateChessEventDto } from '../dtos/create-chess-event.dto';
import { PatchChessEventDto } from '../dtos/patch-chess-event.dto';
import { UpdateChessEventDto } from '../dtos/update-chess-event.dto';
import { ChessEventService } from '../services/chess-event.service';


@ApiTags('ChessEvents')
@Controller('chessEvents')
export class ChessEventsController extends NestedController<
  CreateChessEventDto,
  UpdateChessEventDto,
  PatchChessEventDto,
  ChessEventDto,
  ChessEventNestedDto> {
  constructor(public chessEventService: ChessEventService) {
    super();
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() chessEvent: CreateChessEventDto): Promise<ChessEventDto> {
    return this.chessEventService.create(chessEvent);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  find(@Query() query?: object): Promise<ChessEventDto[]> {
    return this.chessEventService.find(query);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findById(@Param('id') id: string): Promise<ChessEventDto> {
    return this.chessEventService.findById(id);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  findOne(@Query() query?: object): Promise<ChessEventDto> {
    return this.chessEventService.findOne(query);
  }

  @Get(':id/:nested')
  @HttpCode(HttpStatus.OK)
  findNestedDocuments(@Param('id') id: string, @Param('nested') nested: string, @Query() query?: object): Promise<ChessEventNestedDto> {
    return this.chessEventService.findNested(id, nested, query);
  }

  @Put(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  update(@Param('id') id: string, @Body() chessEvent: UpdateChessEventDto): Promise<ChessEventDto> {
    return this.chessEventService.update(id, chessEvent);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  updateProperty(@Param('id') id: string, @Body() partialUser: PatchChessEventDto): Promise<ChessEventDto> {
    return this.chessEventService.updateProperty(id, partialUser);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  deleteDocument(@Param('id') id: string): Promise<void> {
    return this.chessEventService.deleteDocument(id);
  }

  @Patch('restore/:id')
  @HttpCode(HttpStatus.ACCEPTED)
  restoreDocument(@Param('id') id: string): Promise<void> {
    return this.chessEventService.restoreDocument(id);
  }
}
