import { Test, TestingModule } from '@nestjs/testing';
import { ChessEventsController } from './chess-events.controller';

describe('Event Controller', () => {
  let controller: ChessEventsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChessEventsController],
    }).compile();

    controller = module.get<ChessEventsController>(ChessEventsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
