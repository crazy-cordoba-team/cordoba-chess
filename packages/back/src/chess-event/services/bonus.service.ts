import { Injectable } from "@nestjs/common";
import { NestedService } from '../../common/services/nested.service';
import { BonusRepository } from '../repositories/bonus.repository';


@Injectable()
export class BonusService extends NestedService {
  constructor(bonusRepository: BonusRepository) {
    super(bonusRepository);
  }
}
