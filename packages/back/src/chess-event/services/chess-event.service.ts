import { Injectable } from "@nestjs/common";
import { NestedService } from '../../common/services/nested.service';
import { ChessEventRepository } from '../repositories/chess-event.repository';


@Injectable()
export class ChessEventService extends NestedService {
  constructor(chessEventRepository: ChessEventRepository) {
    super(chessEventRepository);
  }
}
