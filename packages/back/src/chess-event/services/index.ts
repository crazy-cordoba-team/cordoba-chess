import { BonusService } from "./bonus.service";
import { ScheduleService } from "./schedule.service";
import { ChessEventService } from "./chess-event.service";

export default [
  BonusService,
  ScheduleService,
  ChessEventService,
];
