import { Test, TestingModule } from '@nestjs/testing';
import { ChessEventService } from './chess-event.service';

describe('EventService', () => {
  let service: ChessEventService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChessEventService],
    }).compile();

    service = module.get<ChessEventService>(ChessEventService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
