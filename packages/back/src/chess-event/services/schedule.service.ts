import { Injectable } from "@nestjs/common";
import { NestedService } from 'src/common/services/nested.service';
import { ScheduleRepository } from '../repositories/schedule.repository';


@Injectable()
export class ScheduleService extends NestedService {
  constructor(private readonly scheduleRepository: ScheduleRepository) {
    super(scheduleRepository);
  }
}
