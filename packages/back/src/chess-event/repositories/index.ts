import { BonusRepository } from './bonus.repository';
import { ChessEventRepository } from './chess-event.repository';
import { ScheduleRepository } from './schedule.repository';

export default [
  BonusRepository,
  ChessEventRepository,
  ScheduleRepository,
]
