import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";
import { NestedRepository } from 'src/common/repositories/nested.repository';
import { ScheduleDto } from '../dtos/schedule.dto';


@Injectable()
export class ScheduleRepository extends NestedRepository {
  constructor(@InjectModel('Schedule') scheduleModel: Model<ScheduleDto>) {
    super(scheduleModel, 'schedule')
  }
}
