import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";
import { NestedRepository } from '../../common/repositories/nested.repository';
import { BonusDto } from '../dtos/bonus.dto';


@Injectable()
export class BonusRepository extends NestedRepository {
  constructor(@InjectModel('Bonus') bonusModel: Model<BonusDto>) {
    super(bonusModel, 'bonus');
  }
}
