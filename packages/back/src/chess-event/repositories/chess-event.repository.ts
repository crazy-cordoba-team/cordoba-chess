import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";
import { NestedRepository } from '../../common/repositories/nested.repository';
import { ChessEventDto } from '../dtos/chess-event.dto';


@Injectable()
export class ChessEventRepository extends NestedRepository {
  constructor(@InjectModel('ChessEvent') chessEventModel: Model<ChessEventDto>) {
    super(chessEventModel, 'chessEvent');
  }
}
