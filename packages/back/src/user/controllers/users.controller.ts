import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post, Put, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { NestedController } from '../../common/controllers/nested.controller';
import { CreateUserDto } from '../dtos/create-user.dto';
import { PatchUserDto } from '../dtos/patch-user.dto';
import { UpdateUserDto } from '../dtos/update-user.dto';
import { UserNestedDto } from '../dtos/user-nested.dto';
import { UserDto } from '../dtos/user.dto';
import { UserService } from '../services/user.service';


@ApiTags('Users')
@Controller('users')
export class UsersController extends NestedController<CreateUserDto, UpdateUserDto, PatchUserDto, UserDto, UserNestedDto> {
  constructor(protected readonly userService: UserService) {
    super();
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() user: CreateUserDto): Promise<UserDto> {
    return this.userService.create(user);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  find(@Query() query?: object): Promise<UserDto[]> {
    return this.userService.find(query);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findById(@Param('id') id: string, @Query() query?: object): Promise<UserDto> {
    return this.userService.findById(id, query);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  findOne(@Query() query?: object): Promise<UserDto> {
    return this.userService.findOne(query);
  }

  @Get(':id/:nested')
  @HttpCode(HttpStatus.OK)
  findNestedDocuments(@Param('id') id: string, @Param('nested') nested: string, @Query() query?: object): Promise<UserNestedDto> {
    return this.userService.findNested(id, nested, query);
  }

  @Put(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  update(@Param('id') id: string, @Body() user: UpdateUserDto): Promise<UserDto> {
    return this.userService.update(id, user);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  updateProperty(@Param('id') id: string, @Body() partialUser: PatchUserDto): Promise<UserDto> {
    return this.userService.updateProperty(id, partialUser);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  deleteDocument(@Param('id') id: string): Promise<void> {
    return this.userService.deleteDocument(id);
  }

  @Patch('restore/:id')
  @HttpCode(HttpStatus.ACCEPTED)
  restoreDocument(@Param('id') id: string): Promise<void> {
    return this.userService.restoreDocument(id);
  }
}
