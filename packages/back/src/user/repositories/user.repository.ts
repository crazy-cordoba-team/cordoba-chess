import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";
import { NestedRepository } from 'src/common/repositories/nested.repository';
import { UserDto } from '../dtos/user.dto';

@Injectable()
export class UserRepository extends NestedRepository {
  constructor(@InjectModel('User') userModel: Model<UserDto>) {
    super(userModel, 'user');

    this.propertiesToPopulate = [
      'club',
      'invitations',
      'publications',
      'schedule',
      'lessons',
    ];
  }
}
