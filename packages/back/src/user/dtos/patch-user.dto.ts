import { IsArray, IsNotEmpty, IsObject, IsString } from "class-validator";
import { ProfileDto } from "./profile.dto";

export class PatchUserDto {
  @IsString()
  public userName?: string;

  @IsString()
  @IsNotEmpty()
  public email?: string;

  @IsString()
  @IsNotEmpty()
  public password?: string;

  @IsObject()
  public profile?: ProfileDto;

  @IsObject()
  public club?: string;

  @IsArray()
  public invitations?: string[];

  @IsArray()
  public publications?: string[];

  @IsArray()
  public Schedule?: string[];

  @IsArray()
  public lessons?: string[]
}
