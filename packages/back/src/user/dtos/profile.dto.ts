import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class ProfileDto {
  @IsString()
  @IsNotEmpty({ message: '$property is required to create a $target' })
  public firstName: string;

  @IsString()
  @IsNotEmpty({ message: '$property is required to create a $target' })
  public lastName: string;

  @IsNumber()
  public age?: number;

  @IsString()
  public celphone?: string;

  @IsString()
  public phone?: string;
}
