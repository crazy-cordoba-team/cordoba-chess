import { IsNotEmpty, IsObject, IsString } from "class-validator";
import { ProfileDto } from './profile.dto';

export class CreateUserDto {
  @IsString()
  public userName?: string;

  @IsString()
  @IsNotEmpty({ message: '$property is required to create a $target' })
  public email: string;

  @IsString()
  @IsNotEmpty({ message: '$property is required to create a $target'})
  public password: string;

  @IsObject()
  public profile?: ProfileDto;
}
