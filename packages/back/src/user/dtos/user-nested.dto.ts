import { Document } from 'mongoose';
import { ClubDto } from 'src/club/dtos/club.dto';
import { InvitationDto } from 'src/club/dtos/invitation.dto';
import { PublicationDto } from 'src/publication/dtos/publication.dto';
import { ScheduleDto } from 'src/chess-event/dtos/schedule.dto';
import { UserDto } from './user.dto';

export class UserNestedDto extends Document {
  club?: ClubDto;
  invitations?: InvitationDto[];
  publications?: PublicationDto[];
  schedule?: ScheduleDto[];
  lessons?: ScheduleDto[];
  deletedBy?: UserDto;
}
