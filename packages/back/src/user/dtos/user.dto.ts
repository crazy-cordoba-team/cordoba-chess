import { IsArray, IsDate, IsNotEmpty, IsObject, IsString } from "class-validator";
import { Document } from 'mongoose';
import { ScheduleDto } from 'src/chess-event/dtos/schedule.dto';
import { ClubDto } from 'src/club/dtos/club.dto';
import { InvitationDto } from 'src/club/dtos/invitation.dto';
import { PublicationDto } from 'src/publication/dtos/publication.dto';
import { ProfileDto } from "./profile.dto";

export class UserDto extends Document {
  @IsString()
  public id?: string;

  @IsString()
  public userName?: string;

  @IsString()
  @IsNotEmpty()
  public email: string;

  @IsString()
  @IsNotEmpty()
  public password: string;

  @IsObject()
  public profile?: ProfileDto;

  public club?: string | ClubDto;

  @IsArray()
  public invitations?: string[] | InvitationDto[];

  @IsArray()
  public publications?: string[] | PublicationDto[];

  @IsArray()
  public Schedule?: string[] | ScheduleDto[];

  @IsArray()
  public lessons?: string[] | ScheduleDto[];

  @IsDate()
  public cratedAt?: Date;

  @IsDate()
  public updatedAt?: Date;

  @IsDate()
  public deletedAt?: Date;

  public deletedBy?: string | UserDto;
}
