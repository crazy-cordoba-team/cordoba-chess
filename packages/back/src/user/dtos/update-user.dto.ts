import { IsArray, IsNotEmpty, IsObject, IsString, ValidateNested } from "class-validator";
import { ProfileDto } from "./profile.dto";

export class UpdateUserDto {
  @IsString()
  public userName: string;

  @IsString()
  @IsNotEmpty()
  public email: string;

  @IsString()
  @IsNotEmpty()
  public password: string;

  @IsObject()
  @ValidateNested()
  public profile: ProfileDto;

  @IsObject()
  public club: string;

  @IsArray()
  public invitations: string[];

  @IsArray()
  public publications: string[];

  @IsArray()
  public Schedule: string[];

  @IsArray()
  public lessons: string[];
}
