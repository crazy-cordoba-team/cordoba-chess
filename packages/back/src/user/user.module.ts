import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import controllers from './controllers';
import repositories from './repositories';
import schemas from './schemas';
import services from './services';

@Module({
  imports: [MongooseModule.forFeature([...schemas])],
  controllers: [...controllers],
  providers: [...repositories, ...services]
})
export class UserModule {}
