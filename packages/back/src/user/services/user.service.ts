import { Injectable } from "@nestjs/common";
import { UserRepository } from "../repositories/user.repository";
import { NestedService } from 'src/common/services/nested.service';


@Injectable()
export class UserService extends NestedService {
  constructor(userRepository: UserRepository) {
    super(userRepository);
  }
}
