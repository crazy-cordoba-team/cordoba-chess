import { Schema } from 'mongoose';


export const ProfileSchema = new Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  age: Number,
  cellphone: String,
  phone: String,
}, {
  timestamps: true,
});
