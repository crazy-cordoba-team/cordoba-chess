import { ProfileSchema } from './profile.schema'
import { UserSchema } from './user.schema'

export default [
  { name: 'User', schema: UserSchema },
  { name: 'Profile', schema: ProfileSchema },
]
