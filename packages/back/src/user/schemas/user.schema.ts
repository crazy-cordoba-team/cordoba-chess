import { Schema } from "mongoose";
import { ProfileSchema } from './profile.schema';


export const UserSchema = new Schema({
  userName: String,
  email: { type: String, required: true, unique: true },
  password: {type: String, required: true },
  profile: ProfileSchema,
  club: { type: Schema.Types.ObjectId, ref: 'Club' },
  invitations: [{type: Schema.Types.ObjectId, ref: 'Invitation'}],
  publications: [{type: Schema.Types.ObjectId, ref: 'Publication'}],
  schedule: [{type: Schema.Types.ObjectId, ref: 'Schedule'}],
  lessons: [{type: Schema.Types.ObjectId, ref: 'Schedule'}],
}, {
  timestamps: true,
});
