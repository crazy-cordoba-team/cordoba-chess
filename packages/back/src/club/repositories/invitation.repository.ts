import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";
import { NestedRepository } from 'src/common/repositories/nested.repository';
import { InvitationDto } from '../dtos/invitation.dto';

@Injectable()
export class InvitationRepository extends NestedRepository {
  constructor(@InjectModel('Invitation') invitationModel: Model<InvitationDto>) {
    super(invitationModel, 'invitation');
  }
}
