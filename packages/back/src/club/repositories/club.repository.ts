import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";
import { NestedRepository } from 'src/common/repositories/nested.repository';
import { ClubDto } from '../dtos/club.dto';

@Injectable()
export class ClubRepository extends NestedRepository {
  constructor(@InjectModel('Club') clubModel: Model<ClubDto>) {
    super(clubModel, 'club');
  }
}
