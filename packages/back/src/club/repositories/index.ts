import { ClubRepository } from './club.repository';
import { InvitationRepository } from './invitation.repository';

export default [
  ClubRepository,
  InvitationRepository,
]
