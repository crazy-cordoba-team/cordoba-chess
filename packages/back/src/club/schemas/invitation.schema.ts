import { Schema } from 'mongoose';


export const InvitationSchema = new Schema({
  title: { type: String, required: true },
  content: { type: String, required: true },
  addressee: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  sender: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  club: { type: Schema.Types.ObjectId, ref: 'Club', required: true },
}, {
  timestamps: true,
});
