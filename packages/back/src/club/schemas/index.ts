import { ClubSchema } from "./club.schema";
import { InvitationSchema } from "./invitation.schema";

export default [
  { name: 'Club', schema: ClubSchema },
  { name: 'Invitation', schema: InvitationSchema },
]
