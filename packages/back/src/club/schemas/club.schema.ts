import { Schema } from 'mongoose';


export const ClubSchema = new Schema({
  name: { type: String, required: true },
  description: String,
  door: { type: String, required: true },
  streetNumber: { type: Number, required: true },
  street: { type: String, required: true },
  city: Object,
  fee: Number,
  partners: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  invitations: [{ type: Schema.Types.ObjectId, ref: 'Investigation' }],
  publications: [{ type: Schema.Types.ObjectId, ref: 'Publication' }],
}, {
  timestamps: true,
});
