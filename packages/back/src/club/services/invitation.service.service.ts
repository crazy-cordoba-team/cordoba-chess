import { Injectable } from "@nestjs/common";
import { NestedService } from 'src/common/services/nested.service';
import { InvitationRepository } from '../repositories/invitation.repository';


@Injectable()
export class InvitationService extends NestedService {
  constructor(invitationRepository: InvitationRepository) {
    super(invitationRepository)
  }
}
