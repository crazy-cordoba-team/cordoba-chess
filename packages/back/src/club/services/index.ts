import { ClubService } from "./club.service";
import { InvitationService } from "./invitation.service.service";

export default [
  ClubService,
  InvitationService,
];
