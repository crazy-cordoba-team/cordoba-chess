import { Injectable } from "@nestjs/common";
import { NestedService } from 'src/common/services/nested.service';
import { ClubRepository } from '../repositories/club.repository';


@Injectable()
export class ClubService extends NestedService {
  constructor(clubRepository: ClubRepository) {
    super(clubRepository);
  }
}
