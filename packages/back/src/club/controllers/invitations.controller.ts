import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post, Put, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { NestedController } from '../../common/controllers/nested.controller';
import { CreateInvitationDto } from '../dtos/create-invitation.dto';
import { InvitationNestedDto } from '../dtos/invitation-nested.dto';
import { InvitationDto } from '../dtos/invitation.dto';
import { PatchInvitationDto } from '../dtos/patch-invitation.dto';
import { UpdateInvitationDto } from '../dtos/update-invitation.dto';
import { InvitationService } from '../services/invitation.service.service';


@ApiTags('Invitations')
@Controller('invitations')
export class InvitationsController extends NestedController<
  CreateInvitationDto,
  UpdateInvitationDto,
  PatchInvitationDto,
  InvitationDto,
  InvitationNestedDto> {
  constructor(public invitationService: InvitationService) {
    super();
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() invitation: CreateInvitationDto): Promise<InvitationDto> {
    return this.invitationService.create(invitation);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  find(@Query() query?: object): Promise<InvitationDto[]> {
    return this.invitationService.find(query);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findById(@Param('id') id: string): Promise<InvitationDto> {
    return this.invitationService.findById(id);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  findOne(@Query() query?: object): Promise<InvitationDto> {
    return this.invitationService.findOne(query);
  }

  @Get(':id/:nested')
  @HttpCode(HttpStatus.OK)
  findNestedDocuments(@Param('id') id: string, @Param('nested') nested: string, @Query() query?: object): Promise<InvitationNestedDto> {
    return this.invitationService.findNested(id, nested, query);
  }

  @Put(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  update(@Param('id') id: string, @Body() invitation: UpdateInvitationDto): Promise<InvitationDto> {
    return this.invitationService.update(id, invitation);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  updateProperty(@Param('id') id: string, @Body() partialUser: PatchInvitationDto): Promise<InvitationDto> {
    return this.invitationService.updateProperty(id, partialUser);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  deleteDocument(@Param('id') id: string): Promise<void> {
    return this.invitationService.deleteDocument(id);
  }

  @Patch('restore/:id')
  @HttpCode(HttpStatus.ACCEPTED)
  restoreDocument(@Param('id') id: string): Promise<void> {
    return this.invitationService.restoreDocument(id);
  }
}
