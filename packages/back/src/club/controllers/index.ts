import { ClubsController } from "./clubs.controller";
import { InvitationsController } from "./invitations.controller";

export default [
  ClubsController,
  InvitationsController,
];
