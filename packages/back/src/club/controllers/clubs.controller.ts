import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post, Put, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { NestedController } from '../../common/controllers/nested.controller';
import { ClubNestedDto } from '../dtos/club-nested.dto';
import { ClubDto } from '../dtos/club.dto';
import { CreateClubDto } from '../dtos/create-club.dto';
import { PatchClubDto } from '../dtos/patch-club.dto';
import { UpdateClubDto } from '../dtos/update-club.dto';
import { ClubService } from '../services/club.service';


@ApiTags('Clubs')
@Controller('clubs')
export class ClubsController extends NestedController<
  CreateClubDto,
  UpdateClubDto,
  PatchClubDto,
  ClubDto,
  ClubNestedDto> {
  constructor(public clubService: ClubService) {
    super();
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() club: CreateClubDto): Promise<ClubDto> {
    return this.clubService.create(club);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  find(@Query() query?: object): Promise<ClubDto[]> {
    return this.clubService.find(query);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findById(@Param('id') id: string): Promise<ClubDto> {
    return this.clubService.findById(id);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  findOne(@Query() query?: object): Promise<ClubDto> {
    return this.clubService.findOne(query);
  }

  @Get(':id/:nested')
  @HttpCode(HttpStatus.OK)
  findNestedDocuments(@Param('id') id: string, @Param('nested') nested: string, @Query() query?: object): Promise<ClubNestedDto> {
    return this.clubService.findNested(id, nested, query);
  }

  @Put(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  update(@Param('id') id: string, @Body() club: UpdateClubDto): Promise<ClubDto> {
    return this.clubService.update(id, club);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  updateProperty(@Param('id') id: string, @Body() partialUser: PatchClubDto): Promise<ClubDto> {
    return this.clubService.updateProperty(id, partialUser);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  deleteDocument(@Param('id') id: string): Promise<void> {
    return this.clubService.deleteDocument(id);
  }

  @Patch('restore/:id')
  @HttpCode(HttpStatus.ACCEPTED)
  restoreDocument(@Param('id') id: string): Promise<void> {
    return this.clubService.restoreDocument(id);
  }
}
