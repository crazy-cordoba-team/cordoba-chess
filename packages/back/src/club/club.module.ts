import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import controllers from './controllers';
import repositories from './repositories';
import entities from './schemas';
import services from './services';

@Module({
  imports: [MongooseModule.forFeature([...entities])],
  providers: [...services, ...repositories],
  controllers: [...controllers],
})
export class ClubModule {}
