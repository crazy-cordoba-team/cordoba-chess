import { Document } from 'mongoose';
import { UserDto } from "src/user/dtos/user.dto";
import { ClubDto } from "./club.dto";


export class InvitationNestedDto extends Document {
  public addressee?: UserDto;
  public sender?: UserDto;
  public club?: ClubDto;
  public deletedBy?: UserDto;
}
