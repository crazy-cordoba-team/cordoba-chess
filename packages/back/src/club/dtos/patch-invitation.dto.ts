import { IsString } from 'class-validator';
import { Document } from 'mongoose';


export class PatchInvitationDto extends Document {
  @IsString()
  public title?: string;

  @IsString()
  public content?: string;

  @IsString()
  public addressee?: string;

  @IsString()
  public sender?: string;

  @IsString()
  public club?: string;
}
