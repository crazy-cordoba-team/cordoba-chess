import { IsArray, IsNotEmpty, IsNumber, IsObject, IsString } from 'class-validator';


export class UpdateClubDto {
  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsString()
  public description?: string;

  @IsString()
  @IsNotEmpty()
  public door: string;

  @IsNumber()
  @IsNotEmpty()
  public streetNumber: number;

  @IsString()
  @IsNotEmpty()
  public street: string;

  @IsObject()
  public city?: object;

  @IsNumber()
  public fee?: number;

  @IsArray()
  public partners?: string[];

  @IsArray()
  public invitations?: string[];

  @IsArray()
  public publications?: string[];
}
