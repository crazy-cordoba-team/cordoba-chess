import { IsString } from 'class-validator';


export class CreateInvitationDto {
  @IsString({ message: '$property is required to create $target' })
  public title: string;

  @IsString({ message: '$property is required to create $target' })
  public content: string;

  @IsString({ message: '$property is required to create $target' })
  public addressee: string;

  @IsString({ message: '$property is required to create $target' })
  public sender: string;

  @IsString({ message: '$property is required to create $target' })
  public club: string;
}
