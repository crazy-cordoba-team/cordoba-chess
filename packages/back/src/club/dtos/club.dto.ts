import { IsArray, IsDate, IsNotEmpty, IsNumber, IsObject, IsString } from 'class-validator';
import { Document } from 'mongoose';
import { PublicationDto } from "src/publication/dtos/publication.dto";
import { UserDto } from "src/user/dtos/user.dto";
import { InvitationDto } from "./invitation.dto";


export class ClubDto extends Document {
  @IsString()
  public id?: string;

  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsString()
  public description?: string;

  @IsString()
  @IsNotEmpty()
  public door: string;

  @IsNumber()
  @IsNotEmpty()
  public streetNumber: number;

  @IsString()
  @IsNotEmpty()
  public street: string;

  @IsObject()
  public city?: object;

  @IsNumber()
  public fee?: number;

  @IsArray()
  public partners?: string[] | UserDto[];

  @IsArray()
  public invitations?: string[] | InvitationDto[];

  @IsArray()
  public publications?: string[] | PublicationDto[];

  @IsDate()
  public createdAt?: Date;

  @IsDate()
  public updatedAt?: Date;

  @IsDate()
  public deletedAt?: Date;

  public deletedBy?: string | UserDto;
}
