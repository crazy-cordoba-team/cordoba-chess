import { IsString, IsNotEmpty } from 'class-validator';
import { Document } from 'mongoose';


export class UpdateInvitationDto extends Document {
  @IsString()
  @IsNotEmpty()
  public title: string;

  @IsString()
  @IsNotEmpty()
  public content: string;

  @IsString()
  @IsNotEmpty()
  public addressee: string;

  @IsString()
  @IsNotEmpty()
  public sender: string;

  @IsString()
  @IsNotEmpty()
  public club: string;
}
