import { Document } from 'mongoose';
import { PublicationDto } from "src/publication/dtos/publication.dto";
import { UserDto } from "src/user/dtos/user.dto";
import { InvitationDto } from "./invitation.dto";


export class ClubNestedDto extends Document {
  public partners?: UserDto[];
  public invitations?: InvitationDto[];
  public publications?: PublicationDto[];
  public deletedBy?: UserDto;
}
