import { IsNotEmpty, IsNumber, IsObject, IsString } from 'class-validator';


export class CreateClubDto {
  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsString()
  public description?: string;

  @IsString()
  @IsNotEmpty()
  public door: string;

  @IsNumber()
  @IsNotEmpty()
  public streetNumber: number;

  @IsString()
  @IsNotEmpty()
  public street: string;

  @IsObject()
  public city?: object;

  @IsNumber()
  public fee?: number;
}
