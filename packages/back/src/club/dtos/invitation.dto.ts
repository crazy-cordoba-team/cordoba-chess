import { IsDate, IsString } from 'class-validator';
import { Document } from 'mongoose';
import { UserDto } from "src/user/dtos/user.dto";
import { ClubDto } from "./club.dto";


export class InvitationDto extends Document {
  @IsString()
  public id?: string;

  @IsString()
  public title: string;

  @IsString()
  public content: string;

  public addressee: string | UserDto;

  public sender: string | UserDto;

  public club: string | ClubDto;

  @IsDate()
  public createdAt?: Date;

  @IsDate()
  public updatedAt?: Date;

  @IsDate()
  public deletedAt?: Date;

  public deletedBy?: string | UserDto;
}
