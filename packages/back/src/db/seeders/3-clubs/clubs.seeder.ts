import * as Faker from 'faker';
import { createModel, mapModelsToInsert } from '../../factory/model.factory';
import { ClubSchema } from '../../../club/schemas/club.schema';

function createClub() {
  return {
    name: Faker.company.companyName(),
    description: Faker.lorem.sentences(4),
    door: Faker.random.alphaNumeric(1),
    streetNumber: Faker.address.streetAddress(),
    street: Faker.address.streetName(),
    city: {
      name: Faker.address.city(),
      state: {
        name: Faker.address.state(),
        country: {
          name: Faker.address.country(),
        }
      }
    },
    fee: Faker.random.number({min: 100, max: 800}),
  }
}

const ClubModel = createModel('Club', ClubSchema);
const clubs = mapModelsToInsert(ClubModel, createClub, 10);

export = clubs;
