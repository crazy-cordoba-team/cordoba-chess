import { CategorySchema } from '../../../publication/schemas/category.schema';
import { createModel, mapModelsToInsert } from '../../factory/model.factory';


const categoriesData = [
  { title: 'tactics', description: 'Articles of tactics' },
  { title: 'strategy', description: 'Articles of strategy' },
  { title: 'opening', description: 'Articles of opening' },
  { title: 'middle game', description: 'Articles of middle game' },
  { title: 'end game', description: 'Articles of end game' },
];

const CategoryModel = createModel('Category', CategorySchema);
const categories = mapModelsToInsert(CategoryModel, categoriesData);

export = categories;
