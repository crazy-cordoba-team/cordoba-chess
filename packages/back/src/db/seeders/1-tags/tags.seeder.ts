import { TagSchema } from '../../../publication/schemas/tag.schema';
import { createModel, mapModelsToInsert } from '../../factory/model.factory';

const tagsData = [
  { name: 'defense '},
  { name: 'attack' },
  { name: 'counter play' },
  { name: 'kings pawn openings' },
  { name: 'queens pawn openings' },
  { name: 'classic game' },
  { name: 'modern game' },
  { name: 'hyper modern game'},
  { name: 'pawn structure' },
  { name: 'bishops pair' },
  { name: 'knights pair' },
  { name: 'open game' },
  { name: 'closed game' },
  { name: 'semi open game' },
];

const TagModel = createModel('Tag', TagSchema);
const tags = mapModelsToInsert(TagModel, tagsData);

export = tags;
