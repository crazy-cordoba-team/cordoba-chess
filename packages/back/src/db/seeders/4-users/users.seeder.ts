
import * as Faker from 'faker';
import { ProfileSchema } from '../../../user/schemas/profile.schema';
import { UserSchema } from '../../../user/schemas/user.schema';
import { createModel, mapModelsToInsert } from '../../factory/model.factory';


Faker.setLocale('es');

function createProfile() {
  return {
    firstName: Faker.name.firstName(),
    lastName: Faker.name.lastName(),
    age: Faker.random.number({ min: 7, max: 85 }),
    cellphone: Faker.phone.phoneNumber(),
    phone: Faker.phone.phoneNumber(),
  }
}

function createUser() {
  const profile = createProfile();
  const ProfileModel = createModel('Profile', ProfileSchema);
  const [profileInstance] = mapModelsToInsert(ProfileModel, [profile]);

  return {
    userName: Faker.internet.userName(profile.firstName, profile.lastName),
    email: Faker.internet.email(profile.firstName, profile.lastName),
    password: Faker.internet.password(8, true),
    profile: profileInstance,
  }
}

const UserModel = createModel('User', UserSchema);
const users = mapModelsToInsert(UserModel, createUser, 20);

export = users;
