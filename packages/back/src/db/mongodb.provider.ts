import { MongooseModule } from '@nestjs/mongoose';
import * as mongooseDelete from 'mongoose-delete';


const username = process.env.MONGO_USERNAME;
const password = process.env.MONGO_PASSWORD;
const database = process.env.MONGO_DATABASE;
const host = process.env.MONGO_HOST;
const port = process.env.MONGO_PORT;
const url = `mongodb://${username}:${password}@${host}:${port}/${database}`

export const mongodbConf = MongooseModule.forRoot(url, {
  useNewUrlParser: true,
  useFindAndModify: false,
  connectionFactory: (connection) => {
    connection.plugin(mongooseDelete, {
      deletedAt: true,
      deletedBy: true,
      overrideMethods: true
    });

    return connection;
  }
});
