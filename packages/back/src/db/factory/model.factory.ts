import { Document, model, Model, Schema } from 'mongoose';

export function createModel(modelName: string, schema: Schema): Model<Document, {}> {
  return model(modelName, schema);
}

export function mapModelsToInsert(
  model: Model<Document, {}>,
  data: any[] | Function,
  count?: number,
): any[] {
  const result = [];
  let dataToCreateModels = [];

  if (Array.isArray(data)) {
    dataToCreateModels = [...data];
  } else if (typeof data === 'function' && count) {
    for (let i = count; i > 0; i--) {
      dataToCreateModels.push(data());
    }
  } else {
    throw new Error('Please use an array of a generator function with a count');
  }

  for (const item of dataToCreateModels) {
    const instance = new model(item);
    result.push(instance.toJSON());
  }

  return result;
}
