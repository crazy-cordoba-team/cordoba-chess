db.createUser({
  user: 'chess',
  pwd: 'chess',
  roles: [
    { role: 'dbAdmin', db: 'cordoba-chess' },
    { role: 'readWrite', db: 'cordoba-chess' }
  ]
});
