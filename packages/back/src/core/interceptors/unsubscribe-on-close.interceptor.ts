import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable, fromEvent } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Request } from 'express';

@Injectable()
export class UnsubscribeOnCloseInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<unknown> {
    if (context.getType() !== 'http') {
      return next.handle();
    }
    const request = context.switchToHttp().getRequest() as Request;
    const close$ = fromEvent(request, 'close');
    return next.handle().pipe(takeUntil(close$));
  }
}
