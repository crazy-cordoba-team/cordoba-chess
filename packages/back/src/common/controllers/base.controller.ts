/**
 * Types
 *
 * Function params:
 * A: CreateSchemaDto
 * B: UpdateSchemaDto
 * C: PatchSchemaDto
 *
 * Functions returns:
 * D: IModel
 * E: IModelNestedDocument
 */
export abstract class BaseController<A, B, C, D> {
  abstract create(createSchemaDto: A): Promise<D>;
  abstract find(query?: object): Promise<D[]>;
  abstract findById(id: string, query?: object): Promise<D>;
  abstract findOne(query: object): Promise<D>;
  abstract update(id: string, updateSchemaDto: B): Promise<D>;
  abstract updateProperty(id: string, patchSchemaDto: C): Promise<D>;
  abstract deleteDocument(id: string): Promise<void>;
  abstract restoreDocument(id: string): Promise<void>;
}
