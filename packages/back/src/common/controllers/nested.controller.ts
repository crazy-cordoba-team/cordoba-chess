import { BaseController } from './base.controller';

/**
 * Types
 *
 * Function params:
 * A: CreateSchemaDto
 * B: UpdateSchemaDto
 * C: PatchSchemaDto
 *
 * Functions returns:
 * D: IModel
 * E: IModelNestedDocument
 */
export abstract class NestedController<A, B, C, D, E> extends BaseController<A, B, C, D> {
  abstract findNestedDocuments(id: string, nested: string, query?: object): Promise<E>;
}
