import { HttpException, HttpStatus } from '@nestjs/common';
import * as moment from 'moment';
import { Model } from 'mongoose';


/**
 * Types
 *
 * Function params:
 * A: CreateSchemaDto
 * B: UpdateSchemaDto
 * C: PatchSchemaDto
 *
 * Functions returns:
 * D: IModel
 */
export abstract class BaseRepository {
  private _defaultPropertiesToPopulate = ['deletedBy'];
  private _propertiesToPopulate = [];

  constructor(protected readonly model: Model<any>, protected readonly schemaName: string) { }

  set propertiesToPopulate(properties: string[]) {
    if (properties.length) {
      const uniqueNewProperties = properties.filter(property => !this._defaultPropertiesToPopulate.includes(property));
      this._propertiesToPopulate = [...uniqueNewProperties];
    }
  }

  get propertiesToPopulate(): string[] {
    return [...this._propertiesToPopulate, ...this._defaultPropertiesToPopulate];
  }

  create<A, D>(createSchemaDto: A): Promise<D> {
    const createdAt = moment().utc().format();
    return new this.model({...createSchemaDto, createdAt}).save();
  }

  find<D>(query?: object): Promise<D[]> {
    const { deleted = false, ...cleanQuery } = query as any;

    if (deleted) {
      return (this.model as any).findDeleted(cleanQuery).exec();
    }

    return this.model.find(cleanQuery).exec();
  }

  async findById<D>(id: string, query?: object): Promise<D> {
    const { nested = false, deleted = false } = query as any;
    let userFound;

    if (deleted) {
      userFound = await (this.model as any).findOneDeleted({ _id: id }).exec();
    } else {
      userFound = await this.model.findById(id).exec();
    }

    if (!userFound) {
      throw new HttpException(`${this.schemaName} with id ${id} not found`, HttpStatus.NOT_FOUND);
    }

    if (nested) {
      this.populateNestedDocuments(userFound);
    }

    return userFound;
  }

  async findOne<D>(query: object): Promise<D> {
    const { nested = false, deleted = false, ...cleanedQuery } = query as any;
    let userFound;

    if (deleted) {
      userFound = await (this.model as any).findOneDeleted(cleanedQuery).exec();
    } else {
      userFound = await this.model.findOne(cleanedQuery).exec();
    }

    if (!userFound) {
      throw new HttpException(`${this.schemaName} not found with query ${cleanedQuery}`, HttpStatus.NOT_FOUND);
    }

    if (nested) {
      this.populateNestedDocuments(userFound);
    }

    return userFound;
  }

  update<B, D>(id: string, updateSchemaDto: B): Promise<D> {
    const updatedAt = moment().utc().format();
    return this.model.findByIdAndUpdate(id, { ...updateSchemaDto, updatedAt }).exec();
  }

  async updateProperty<C, D>(id: string, patchSchemaDto: C): Promise<D> {
    const updatedAt = moment().utc().format();
    const modelFound = await this.model.findById(id).exec();

    if (!modelFound) {
      throw new HttpException(`${this.schemaName} with id ${id} not found, imposible to patch it`, HttpStatus.NOT_FOUND);
    }

    return this.model.findByIdAndUpdate(id, { ...(modelFound as any).toJSON(), ...patchSchemaDto, updatedAt }).exec();
  }

  async deleteDocument(id: string): Promise<void> {
    const userToDelete = await this.model.findById(id).exec();

    if (!userToDelete) {
      throw new HttpException(`${this.schemaName} to delete not found`, HttpStatus.NOT_FOUND);
    }

    (userToDelete as any).delete();
  }

  async restoreDocument(id: string): Promise<void> {
    const userToRestore = await (this.model as any).findOneDeleted({ _id: id }).exec();

    if (!userToRestore) {
      throw new HttpException(`${this.schemaName} to restore not found`, HttpStatus.NOT_FOUND);
    }

    await (this.model as any).restore({ _id: (userToRestore as any).id }).exec();
  }

  protected populateNestedDocuments(model: any): void {
    this.propertiesToPopulate.forEach(async (prop) => {
      await model.populate(prop).execPopulate();
    });
  }
}
