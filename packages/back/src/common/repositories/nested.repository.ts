import { HttpException, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { BaseRepository } from './base.repository';


/**
 * Types
 *
 * Functions returns:
 * E: IModelNestedDocument
 */
export abstract class NestedRepository extends BaseRepository {
  constructor(model: Model<any>, schemaName: string) {
    super(model, schemaName);
  }

  async findNested<E>(id: string, nested: string, query?: object): Promise<E> {
    const { deleted = false } = query as any;
    let userFound;

    if (deleted) {
      userFound = await (this.model as any).findOneDeleted({ _id: id })
        .populate(nested)
        .exec();
    } else {
      userFound = await this.model.findById(id)
        .populate(nested)
        .exec();
    }

    if (!userFound) {
      throw new HttpException(`${nested} document in ${this.schemaName} with id ${id} not found`, HttpStatus.NOT_FOUND);
    }

    return userFound[nested];
  }
}
