import { BaseRepository } from '../repositories/base.repository';
import { NestedRepository } from '../repositories/nested.repository';


/**
 * Types
 *
 * Function params:
 * A: CreateSchemaDto
 * B: UpdateSchemaDto
 * C: PatchSchemaDto
 *
 * Functions returns:
 * D: IModel
 * E: IModelNestedDocument
 */
export class BaseService<T extends BaseRepository | NestedRepository> {
  constructor(protected readonly repository: T extends NestedRepository ? NestedRepository : BaseRepository) {}

  create<A, D>(createSchemaDto: A): Promise<D> {
    return this.repository.create<A, D>(createSchemaDto);
  }

  find<D>(query?: object): Promise<D[]> {
    return this.repository.find<D>(query);
  }

  findById<D>(id: string, query?: object): Promise<D> {
    return this.repository.findById<D>(id, query);
  }

  findOne<D>(query?: object): Promise<D> {
    return this.repository.findOne<D>(query);
  }

  update<B, D>(id: string, updateSchemaDto: B): Promise<D> {
    return this.repository.update<B, D>(id, updateSchemaDto);
  }

  updateProperty<C, D>(id: string, patchSchemaDto: C): Promise<D> {
    return this.repository.updateProperty<C, D>(id, patchSchemaDto);
  }

  deleteDocument(id: string): Promise<void> {
    return this.repository.deleteDocument(id);
  }

  restoreDocument(id: string): Promise<void> {
    return this.repository.restoreDocument(id);
  }
}
