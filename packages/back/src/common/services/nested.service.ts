import { NestedRepository } from '../repositories/nested.repository';
import { BaseService } from './base.service';


/**
 * Types
 *
 * Functions returns:
 * E: IModelNestedDocument
 */
export class NestedService extends BaseService<NestedRepository> {
  constructor(repository: NestedRepository) {
    super(repository);
  }

  findNested<E>(id: string, nested: string, query?: object): Promise<E> {
    return this.repository.findNested<E>(id, nested, query);
  }
}
