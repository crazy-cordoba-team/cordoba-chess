import { IsArray, IsObject } from 'class-validator';
import { Document } from 'mongoose';
import { ClubDto } from '../../club/dtos/club.dto';
import { UserDto } from "../../user/dtos/user.dto";
import { CategoryDto } from "./category.dto";
import { TagDto } from './tag.dto';

export class PublicationNestedDto extends Document {
  @IsObject()
  public author?: UserDto;

  @IsObject()
  public club?: ClubDto;

  @IsArray()
  public categories?: CategoryDto[];

  @IsArray()
  public tags?: TagDto[];

  @IsObject()
  public deletedBy?: UserDto;
}
