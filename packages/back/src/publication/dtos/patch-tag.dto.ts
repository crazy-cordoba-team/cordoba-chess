import { IsNotEmpty, IsString } from "class-validator";


export class PatchTagDto {
  @IsString()
  @IsNotEmpty()
  public name?: string;
}
