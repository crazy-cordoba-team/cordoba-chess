import { IsDate, IsNotEmpty, IsString } from "class-validator";
import { Document } from 'mongoose';
import { UserDto } from 'src/user/dtos/user.dto';


export class TagDto extends Document {
  @IsString()
  public id?: string;

  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsDate()
  public createdAt?: Date;

  @IsDate()
  public updatedAt?: Date;

  @IsDate()
  public deletedAt?: Date;

  @IsString()
  public deletedBy?: string | UserDto;
}
