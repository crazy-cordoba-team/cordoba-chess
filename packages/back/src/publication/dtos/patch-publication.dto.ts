import { IsArray, IsNotEmpty, IsString } from 'class-validator';

export class PatchPublicationDto {
  @IsString()
  @IsNotEmpty()
  public title?: string;

  @IsString()
  @IsNotEmpty()
  public content?: string;

  @IsString()
  @IsNotEmpty()
  public author?: string;

  @IsString()
  @IsNotEmpty()
  public club?: string;

  @IsArray()
  public categories?: string[];

  @IsArray()
  public tags?: string[];
}
