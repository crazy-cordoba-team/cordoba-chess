import { IsNotEmpty, IsString } from 'class-validator';

export class CreateTagDto {
  @IsString()
  @IsNotEmpty({ message: '$property is required to create a $target'})
  public name: string;
}
