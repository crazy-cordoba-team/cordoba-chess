import { IsString, IsNotEmpty } from 'class-validator';

export class UpdateCategoryDto {
  @IsString()
  @IsNotEmpty({ message: '$property is required to build a $target' })
  public title: string;

  @IsString()
  public description?: string;
}
