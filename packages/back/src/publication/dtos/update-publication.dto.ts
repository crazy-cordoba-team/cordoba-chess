import { IsArray, IsNotEmpty, IsString } from 'class-validator';

export class UpdatePublicationDto {
  @IsString()
  @IsNotEmpty({ message: '$property is required to update $target' })
  public title: string;

  @IsString()
  @IsNotEmpty({ message: '$property is required to update $target' })
  public content: string;

  @IsString()
  @IsNotEmpty({ message: '$property is required to update $target' })
  public author: string;

  @IsString()
  @IsNotEmpty({ message: '$property is required to update $target' })
  public club: string;

  @IsArray()
  public categories?: string[];

  @IsArray()
  public tags?: string[];
}
