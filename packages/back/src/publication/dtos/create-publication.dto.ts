import { IsArray, IsNotEmpty, IsString } from 'class-validator';

export class CreatePublicationDto {
  @IsString()
  @IsNotEmpty({ message: '$property is required to create $target' })
  public title: string;

  @IsString()
  @IsNotEmpty({ message: '$property is required to create $target' })
  public content: string;

  @IsString()
  @IsNotEmpty({ message: '$property is required to create $target' })
  public author: string;

  @IsString()
  @IsNotEmpty({ message: '$property is required to create $target' })
  public club: string;

  @IsArray()
  public categories?: string[];

  @IsArray()
  public tags?: string[];
}
