import { IsString, IsNotEmpty } from 'class-validator';

export class PatchCategoryDto {
  @IsString()
  @IsNotEmpty()
  public title?: string;

  @IsString()
  public description?: string;
}
