import { IsDate, IsNotEmpty, IsString } from "class-validator";
import { Document } from 'mongoose';
import { UserDto } from 'src/user/dtos/user.dto';

export class CategoryDto extends Document {
  @IsString()
  public id?: string;

  @IsString()
  @IsNotEmpty()
  public title: string;

  @IsString()
  public description?: string;

  @IsDate()
  public createdAt?: Date;

  @IsDate()
  public updatedAt?: Date;

  @IsDate()
  public deletedAt?: Date;

  @IsString()
  public deletedBy?: string | UserDto;
}
