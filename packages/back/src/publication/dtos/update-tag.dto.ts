import { IsNotEmpty, IsString } from "class-validator";


export class UpdateTagDto {
  @IsString()
  @IsNotEmpty({ message: '$property is required to update a $target' })
  public name: string;
}
