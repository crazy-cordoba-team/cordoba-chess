import { IsArray, IsDate, IsNotEmpty, IsObject, IsString } from 'class-validator';
import { Document } from 'mongoose';
import { ClubDto } from '../../club/dtos/club.dto';
import { UserDto } from "../../user/dtos/user.dto";
import { CategoryDto } from "./category.dto";
import { TagDto } from './tag.dto';

export class PublicationDto extends Document {
  @IsString()
  public id?: string;

  @IsString()
  @IsNotEmpty()
  public title: string;

  @IsString()
  @IsNotEmpty()
  public content: string;

  @IsNotEmpty()
  public author: string;

  public club?: string;

  @IsArray()
  public categories?: string[];

  @IsArray()
  public tags?: string[];

  @IsDate()
  public createdAt?: Date;

  @IsDate()
  public updatedAt?: Date;

  @IsDate()
  public deletedAt?: Date;

  public deletedBy?: string;
}
