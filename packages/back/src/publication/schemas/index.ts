import { CategorySchema } from "./category.schema";
import { PublicationSchema } from "./publication.schema";
import { TagSchema } from "./tag.schema";

export default [
  { name: 'Tag', schema: TagSchema },
  { name: 'Category', schema: CategorySchema },
  { name: 'Publication', schema: PublicationSchema },
];
