import { AbstractPublicationContentSchema } from "./publication-content.schema";


export const PublicationSchema = new AbstractPublicationContentSchema({}, { timestamps: true });
