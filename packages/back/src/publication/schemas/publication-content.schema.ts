import { Schema } from 'mongoose';
import { inherits } from 'util';

export function AbstractPublicationContentSchema(...args) {
  //call super
  Schema.apply(this, args);

  //add
  this.add({
    title: { type: String, required: true },
    content: { type: String, required: true },
    author: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    club: { type: Schema.Types.ObjectId, ref: 'Club', required: true },
    categories: [{type: Schema.Types.ObjectId, ref: 'Category'}],
    tags: [{type: Schema.Types.ObjectId, ref: 'Tag'}],
  });
};

inherits(AbstractPublicationContentSchema, Schema);
