import { Schema } from 'mongoose';


export const CategorySchema = new Schema({
  title: { type: String, required: true, unique: true },
  description: { type: String, required: true },
  createdAt: Date,
  updatedAt: Date,
  deletedAt: Date,
}, {
  timestamps: true,
});
