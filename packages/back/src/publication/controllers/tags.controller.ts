import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post, Put, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../common/controllers/base.controller';
import { CreateTagDto } from '../dtos/create-tag.dto';
import { PatchTagDto } from '../dtos/patch-tag.dto';
import { TagDto } from '../dtos/tag.dto';
import { UpdateTagDto } from '../dtos/update-tag.dto';
import { TagService } from '../services/tag.service';


@ApiTags('Tags')
@Controller('tags')
export class TagsController extends BaseController<CreateTagDto, UpdateTagDto, PatchTagDto, TagDto> {
  constructor(public tagService: TagService) {
    super();
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() tag: CreateTagDto): Promise<TagDto> {
    return this.tagService.create(tag);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  find(@Query() query?: object): Promise<TagDto[]> {
    return this.tagService.find(query);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findById(@Param('id') id: string): Promise<TagDto> {
    return this.tagService.findById(id);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  findOne(@Query() query?: object): Promise<TagDto> {
    return this.tagService.findOne(query);
  }

  @Put(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  update(@Param('id') id: string, @Body() tag: UpdateTagDto): Promise<TagDto> {
    return this.tagService.update(id, tag);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  updateProperty(@Param('id') id: string, @Body() partialUser: PatchTagDto): Promise<TagDto> {
    return this.tagService.updateProperty(id, partialUser);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  deleteDocument(@Param('id') id: string): Promise<void> {
    return this.tagService.deleteDocument(id);
  }

  @Patch('restore/:id')
  @HttpCode(HttpStatus.ACCEPTED)
  restoreDocument(@Param('id') id: string): Promise<void> {
    return this.tagService.restoreDocument(id);
  }
}
