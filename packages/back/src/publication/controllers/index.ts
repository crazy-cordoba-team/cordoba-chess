import { PublicationsController } from './publications.controller'
import { CategoriesController } from './categories.controller'
import { TagsController } from './tags.controller'

export default [
  PublicationsController,
  CategoriesController,
  TagsController
];
