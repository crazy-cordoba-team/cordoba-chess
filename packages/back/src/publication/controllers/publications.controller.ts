import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post, Put, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { NestedController } from '../../common/controllers/nested.controller';
import { CreatePublicationDto } from '../dtos/create-publication.dto';
import { PatchPublicationDto } from '../dtos/patch-publication.dto';
import { PublicationNestedDto } from '../dtos/publication-nested.dto';
import { PublicationDto } from '../dtos/publication.dto';
import { UpdatePublicationDto } from '../dtos/update-publication.dto';
import { PublicationService } from '../services/publication.service';


@ApiTags('Publications')
@Controller('publications')
export class PublicationsController extends NestedController<
  CreatePublicationDto,
  UpdatePublicationDto,
  PatchPublicationDto,
  PublicationDto,
  PublicationNestedDto> {
  constructor(public publicationService: PublicationService) {
    super();
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() publication: CreatePublicationDto): Promise<PublicationDto> {
    return this.publicationService.create(publication);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  find(@Query() query?: object): Promise<PublicationDto[]> {
    return this.publicationService.find(query);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findById(@Param('id') id: string): Promise<PublicationDto> {
    return this.publicationService.findById(id);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  findOne(@Query() query?: object): Promise<PublicationDto> {
    return this.publicationService.findOne(query);
  }

  @Get(':id/:nested')
  @HttpCode(HttpStatus.OK)
  findNestedDocuments(@Param('id') id: string, @Param('nested') nested: string, @Query() query?: object): Promise<PublicationNestedDto> {
    return this.publicationService.findNested(id, nested, query);
  }

  @Put(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  update(@Param('id') id: string, @Body() publication: UpdatePublicationDto): Promise<PublicationDto> {
    return this.publicationService.update(id, publication);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  updateProperty(@Param('id') id: string, @Body() partialUser: PatchPublicationDto): Promise<PublicationDto> {
    return this.publicationService.updateProperty(id, partialUser);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.ACCEPTED)
  deleteDocument(@Param('id') id: string): Promise<void> {
    return this.publicationService.deleteDocument(id);
  }

  @Patch('restore/:id')
  @HttpCode(HttpStatus.ACCEPTED)
  restoreDocument(@Param('id') id: string): Promise<void> {
    return this.publicationService.restoreDocument(id);
  }
}
