import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";
import { BaseRepository } from 'src/common/repositories/base.repository';
import { CategoryDto } from '../dtos/category.dto';

@Injectable()
export class CategoryRepository extends BaseRepository {
  constructor(@InjectModel('Category') categoryModel: Model<CategoryDto>) {
    super(categoryModel, 'category');
  }
}
