import { CategoryRepository } from './category.repository';
import { PublicationRepository } from './publication.repository';
import { TagRepository } from './tag.repository';

export default [
  CategoryRepository,
  PublicationRepository,
  TagRepository,
]
