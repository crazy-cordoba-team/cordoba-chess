import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";
import { NestedRepository } from 'src/common/repositories/nested.repository';
import { PublicationDto } from '../dtos/publication.dto';

@Injectable()
export class PublicationRepository extends NestedRepository {
  constructor(@InjectModel('Publication') publicationModel: Model<PublicationDto>) {
    super(publicationModel, 'publication');
  }
}
