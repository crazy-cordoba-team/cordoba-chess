import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";
import { BaseRepository } from 'src/common/repositories/base.repository';
import { TagDto } from '../dtos/tag.dto';

@Injectable()
export class TagRepository extends BaseRepository {
  constructor(@InjectModel('Tag') tagModel: Model<TagDto>) {
    super(tagModel, 'tag');
  }
}
