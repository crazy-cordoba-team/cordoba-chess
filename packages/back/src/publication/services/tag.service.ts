import { Injectable } from "@nestjs/common";
import { BaseRepository } from 'src/common/repositories/base.repository';
import { BaseService } from 'src/common/services/base.service';
import { TagRepository } from '../repositories/tag.repository';


@Injectable()
export class TagService extends BaseService<BaseRepository> {
  constructor(tagRepository: TagRepository) {
    super(tagRepository);
  }
}
