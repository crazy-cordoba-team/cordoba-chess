import { Injectable } from "@nestjs/common";
import { BaseRepository } from 'src/common/repositories/base.repository';
import { BaseService } from 'src/common/services/base.service';
import { CategoryRepository } from '../repositories/category.repository';


@Injectable()
export class CategoryService extends BaseService<BaseRepository> {
  constructor(categoryRepository: CategoryRepository) {
    super(categoryRepository)
  }
}
