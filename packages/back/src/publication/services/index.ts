import { PublicationService } from "./publication.service";
import { CategoryService } from "./category.service";
import { TagService } from "./tag.service";

export default [
  PublicationService,
  CategoryService,
  TagService,
];
