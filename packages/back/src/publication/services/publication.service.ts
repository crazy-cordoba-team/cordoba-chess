import { Injectable } from "@nestjs/common";
import { NestedService } from 'src/common/services/nested.service';
import { PublicationRepository } from '../repositories/publication.repository';


@Injectable()
export class PublicationService extends NestedService {
  constructor(publicationRepository: PublicationRepository) {
    super(publicationRepository);
  }
}
