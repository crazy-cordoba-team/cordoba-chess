import { Module } from '@nestjs/common';
import { ClubModule } from './club/club.module';
import { mongodbConf } from './db/mongodb.provider';
import { EventModule } from './chess-event/event.module';
import { PublicationModule } from './publication/publication.module';
import { UserModule } from './user/user.module';


@Module({
  imports: [
    mongodbConf,
    EventModule,
    ClubModule,
    PublicationModule,
    UserModule,
  ],
})
export class AppModule {}
