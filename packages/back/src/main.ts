import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { UnsubscribeOnCloseInterceptor } from './core/interceptors/unsubscribe-on-close.interceptor';
import * as packageJson from '../package.json';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalInterceptors(new UnsubscribeOnCloseInterceptor());

  const options = new DocumentBuilder()
    .setTitle('Cordoba Chess')
    .setDescription('The Cordoba Chess API')
    .setVersion(packageJson.version)
    .addTag('Resource endpoints')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(process.env.NEST_PORT);
}
bootstrap();
