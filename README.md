# cordoba-chess

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)
[![yarn](https://img.shields.io/badge/yarn-v1.17.3-blue)][7]
[![node](https://img.shields.io/badge/node-v10.16.0-green)][8]
[![vscode](https://img.shields.io/badge/editor-vscode-blue)][9]
[![discord](https://img.shields.io/discord/676070609989664768)][16]

## Important tools to know

This project works with three important tools to know

 - **[Yarn][7]**: This project use <img src="https://storage.googleapis.com/hocicoscuriosos-952ab.appspot.com/ico-yarn.svg" alt="yarn" width="32" height="16" />, please don't use ![npm](https://storage.googleapis.com/hocicoscuriosos-952ab.appspot.com/ico-npm.svg)
 - **[Lerna][1]**: This greet tool help us managin the entire monorepo.
 - **[Husky][2]**: This is a git hook tool, it help us running a few tasks before commiting any change in the repository.
 - **[Lint-stage][3]**: This tool help us linting all files in the stage area of git before commit it, this is in order to prevent commit basic errors in the code style.
 - **[Commitlint][4]**: This tool help us defining a standarization of all commits in the repo in order to extract all changes added in every release using only the commit message.
 - **[Discord][16]**: This tool help us to discus all in order.
 - **[Docker][17]**: This tool help us creating a development environment.
 - **[Docker Compose][18]**: This tool help us creating services related between them.

For more information about this tools please see your repositories.

## Standards

We use the following standards

- **[Restful API][19]**: In this web you found all about restful api standard

## Frameworks

**Backend**: We use [Nestjs][12] with [typescript][15]

**Frontend** We use [VueJs v2][13] with [Quasar framework v1][14] or later, when VueJs v3 is released we will chenge to it in order to use [typescript][15] in the frontend

## Contribution guide

Please see the [contribution guide][5] for a detailed information.

## Style guide

**Frontend**: In frontend package we use a [airbnb style guide][10], please read this guide if you have any doubt.

**Backend**: In backend package we use a [nestjs style guide][11], please read the documentation if you have any doubt.

## Changelog

Plese see the [changelog][6] report.

[1]: https://github.com/lerna/lerna#about
[2]: https://github.com/typicode/husky
[3]: https://github.com/okonet/lint-staged
[4]: https://github.com/conventional-changelog/commitlint
[5]: ./CONTRIBUTING.md
[6]: ./CHANGELOG.md
[7]: https://yarnpkg.com/es-ES/
[8]: https://nodejs.org/en/
[9]: https://code.visualstudio.com/
[10]: https://github.com/airbnb/javascript/blob/master/README.md
[11]: https://docs.nestjs.com/
[12]: https://nestjs.com/
[13]: https://vuejs.org/v2/guide/
[14]: https://quasar.dev/start/pick-quasar-flavour
[15]: https://www.typescriptlang.org/docs/handbook/basic-types.html
[16]: https://discord.gg/6tsdfr5
[17]: https://docs.docker.com/install/
[18]: https://docs.docker.com/compose/install/
[19]: https://restfulapi.net/
